import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_rsync_installed(host):
    assert host.package("rsync").is_installed


def test_rsync_port_listening(host):
    assert host.socket("tcp://873").is_listening
