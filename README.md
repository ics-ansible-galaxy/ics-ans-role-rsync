ics-ans-role-rsync
===================

Ansible role to install rsync and configure rsyncd.conf for rsync daemon.

Rsync copies/sends/backups data between two points by sending only the difference of files from source to destination.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Example Playbook
----------------

```yaml
 - hosts: servers
   roles:
    - role: ics-ans-role-rsync

```

Role Variables
--------------
```yaml
---
# defaults file for rsync-server
rsync_server_uid: root
rsync_server_gid: root
rsync_server_chroot: true
rsync_server_symlinks: false
rsync_server_list: false
rsync_server_exclude_compress: "*"
rsync_server_log_path: ""  # sends to syslog, to store locally specify path ex. /var/log/rsyncd.log
rsync_server_log_transfer: true
rsync_server_log_format: "%o %h [%a] %f %'l"
rsync_server_pid_path: /var/run/rsyncd.pid
rsync_server_readonly: false
# rsync_server_timeout: 0  # 0 by default, indicates unlimited.
rsync_server_max_connections: "0"  # indicates unlimited.
rsync_server_shares: []
# - name: client_hostname
#   path: "poolname/client_hostname"
#   hosts_allow: 10.4.3.15, 172.30.4.0/24, client_hostname.cslab.esss.lu.se
#   options:
#     includes: "nothing"
#     excludes: "lost+found/ test/ .*"
#     exclude_compress:
#     timeout: 600
#     comment: "testing client 1"
# - name: client2
#   path: "poolname/client2"
#   hosts_allow: client2.cslab.esss.lu.se client_hostname.cslab.esss.lu.se
# - name: client3
#   path: "poolname/client3"
#   hosts_allow: client3.cslab.esss.lu.se
#   options:
#     excludes: "lost+found/ test/ .*"



License
-------

BSD 2-clause
